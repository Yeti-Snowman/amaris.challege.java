package amaris.challege.dto;

public class Comment {
	private Integer userId;
	private Integer id;
	private String text;

	public Comment(Integer id, String text, Integer userId) {
		this.setId(id);
		this.setText(text);
		this.setUserId(userId);
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
