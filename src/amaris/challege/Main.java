package amaris.challege;

import amaris.challege.service.UserService;
import amaris.challege.service.impl.UserServiceImpl;

public class Main {
	public static void main(String[] args) {
		UserService userService = new UserServiceImpl();
		Challege app = new Challege(userService);
		app.run();
	}
}
