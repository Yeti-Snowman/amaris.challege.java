package amaris.challege;

import java.util.ArrayList;
import java.util.List;

import amaris.challege.dto.User;
import amaris.challege.service.UserService;

public class Challege {

	private UserService userService;

	public Challege(UserService userService) {
		this.userService = userService;
	}

	public void run() {
		List<User> users = this.userService.createUser(new ArrayList<User>());
		System.out.println(users.get(0).getComments().get(0).getText());
	}

}
