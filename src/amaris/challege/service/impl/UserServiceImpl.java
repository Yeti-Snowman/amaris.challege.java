package amaris.challege.service.impl;

import java.util.ArrayList;
import java.util.List;

import amaris.challege.dto.Comment;
import amaris.challege.dto.User;
import amaris.challege.repository.Repository;
import amaris.challege.service.UserService;

public class UserServiceImpl implements UserService {

	@Override
	public List<User> createUser(ArrayList<User> users) {
		if (users == null) {
			users = new ArrayList<User>();
		}
		User user = new User();
		
		Integer id = users.size() + 1;
		user.setId(id);
		
		List<Comment> comments = Repository.getComments(id);
		user.setComments(comments);
		
		users.add(user);
		return users;
	}

}
