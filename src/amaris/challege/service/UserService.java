package amaris.challege.service;

import java.util.ArrayList;
import java.util.List;

import amaris.challege.dto.User;

public interface UserService {

	List<User> createUser(ArrayList<User> arrayList);

}
